KLGMAT001 - Assignment 4

A simple image manipulation program is created here.

To run the program:
-Navigate to /src
-Type 'make' and hit return

The program supports a number of operations. These are selectable by applying the 
approprriate input parameters.
The program is run using the followoing form

./driver <operation> <input file> <input file> <outputfile>

-Add
    ./driver -a <image1.pgm> <image2.pgm> <outputname.pgm>
-Subtract
    ./driver -s <image1.pgm> <image2.pgm> <outputname.pgm>
-Invert
    ./driver -i <image1.pgm> <outputname.pgm>
-Mask
    ./driver -l <image1.pgm> <image2.pgm> <outputname.pgm>
threshhold
    ./driver -f <image1.pgm> <int threshhold> <outputname.pgm>

Source images must be placed in /Images
Output files are stored in /out
<image> must be of the form, imagename.pgm, with the extension.
This applied to the output file as well.

No filtering has been implemented.

UNIT TEST
To run the unit test:
- navigate to /src
- type 'make test'
- ./test