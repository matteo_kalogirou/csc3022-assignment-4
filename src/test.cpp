#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "imageops.h"

using namespace std;


TEST_CASE("Vector Constructor", ""){
    //Create vector
    vector<unsigned char>  v{0, 1, 2, 50, 150, 255};
    Image *i = new Image(v);

    REQUIRE(i->getH() == 6);
    REQUIRE(i->getW() == 1);
    REQUIRE(i->get(0) == 0);
    REQUIRE(i->get(1) == 1);
    REQUIRE(i->get(2) == 2);
    REQUIRE(i->get(3) == 50);
    REQUIRE(i->get(4) == 150);
    REQUIRE(i->get(5) == 255);
}

TEST_CASE("Iterator ++", ""){
    //Create vector
    vector<unsigned char>  v{48, 49, 50, 51, 150, 255};
    Image *i = new Image(v);
    Image::iterator beg = i->begin();
    Image::iterator end = i->end();

    REQUIRE( *beg == 48 );
    ++beg;
    REQUIRE( *beg == 49 );
    ++beg;
    REQUIRE( *beg == 50 );
    ++beg;
    REQUIRE( *beg == 51 );
    ++beg;
    REQUIRE( *beg == 150 );
    ++beg;
    REQUIRE( *beg == 255 );
    ++beg;
    REQUIRE( *beg == *end);
}

TEST_CASE("Iterator --", ""){
    //Create vector
    vector<unsigned char>  v{48, 49, 50, 51, 150, 255};
    Image *i = new Image(v);
    Image::iterator beg = i->end();
    Image::iterator end = i->begin();

    --beg;
    REQUIRE( *beg == 255 );
    --beg;
    REQUIRE( *beg == 150 );
    --beg;
    REQUIRE( *beg == 51 );
    --beg;
    REQUIRE( *beg == 50 );
    --beg;
    REQUIRE( *beg == 49 );
    --beg;
    REQUIRE( *beg == 48 );
    REQUIRE( *beg == *end);
}

 TEST_CASE("Copy Construction", ""){
    vector<unsigned char>  v{0, 1, 2, 50, 150, 255};
    Image *i = new Image(v);
    Image *j(i);

     REQUIRE( i->getH() == j->getH() );
     REQUIRE( i->getW() == j->getW() );
     REQUIRE( i->get(0) == j->get(0) );
     REQUIRE( i->get(1) == j->get(1) );
     REQUIRE( i->get(2) == j->get(2) );
     REQUIRE( i->get(3) == j->get(3) );
     REQUIRE( i->get(4) == j->get(4) );
     REQUIRE( i->get(5) == j->get(5) );
}

 TEST_CASE("Copy Assignment", ""){
    vector<unsigned char>  v{0, 1, 2, 50, 150, 255};
    vector<unsigned char>  b{2, 33, 34, 55, 157, 245};
    Image *i = new Image(v);
    Image *j = new Image(b);
    j = i;

     REQUIRE( i->getH() == j->getH() );
     REQUIRE( i->getW() == j->getW() );
     REQUIRE( i->get(0) == j->get(0) );
     REQUIRE( i->get(1) == j->get(1) );
     REQUIRE( i->get(2) == j->get(2) );
     REQUIRE( i->get(3) == j->get(3) );
     REQUIRE( i->get(4) == j->get(4) );
     REQUIRE( i->get(5) == j->get(5) );
}

 TEST_CASE("Overloading / - Mask", ""){
    vector<unsigned char>  v{2, 1, 2, 50, 150, 255};
    vector<unsigned char>  b{0, 0, 34, 0, 157, 0};
    Image *i = new Image(v);
    Image *j = new Image(b);

    *i / *j;

     REQUIRE( i->get(0) == 0 );
     REQUIRE( i->get(1) == 0 );
     REQUIRE( i->get(2) == 2 );
     REQUIRE( i->get(3) == 0 );
     REQUIRE( i->get(4) == 150 );
     REQUIRE( i->get(5) == 0 );
}


TEST_CASE("Overloaded * - Threshhold"){
    vector<unsigned char>  v{0, 1, 2, 50, 150, 255};
    Image *i = new Image(v);
    int thresh = 100;

    *i * thresh;

    REQUIRE( i->get(0) == 0);
    REQUIRE( i->get(1) == 0);
    REQUIRE( i->get(2) == 0);
    REQUIRE( i->get(3) == 0);
    REQUIRE( i->get(4) == 150);
    REQUIRE( i->get(5) == 255);
}

TEST_CASE("Overloaded !",""){
    vector<unsigned char>  b{2, 33, 34, 55, 157, 245};
    Image *i = new Image(b);

    !*i;

    REQUIRE( i->get(0) == 253 );
    REQUIRE( i->get(1) == 222 );
    REQUIRE( i->get(2) == 221 );
    REQUIRE( i->get(3) == 200 );
    REQUIRE( i->get(4) == 98 );
    REQUIRE( i->get(5) == 10 );
}