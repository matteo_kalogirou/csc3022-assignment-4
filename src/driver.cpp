#include "imageops.h"

using namespace std;


int main(int argc, char * const argv[])
{

    string l1,l2;   // Input images names
    string op;      // Operation
    string o1;      // Output file name

    if(argc < 2) {cout << "Not enough input arguments."; return 0;}
    op = argv[1];

    char operation = op[1];

    cout << "Operation = " << operation << endl;
    
    switch(operation){
        case 'a':{
            //Done
            l1 = argv[2];
            l2 = argv[3];
            o1 = argv[4];
            Image *a = new Image(l1);
            Image *b = new Image(l2);
            Image *c(a);
            *c += *b;
            c->save(o1);
            break;
        }
        case 's':{
            //Done
            l1 = argv[2];
            l2 = argv[3];
            o1 = argv[4];
            Image *a = new Image(l1);
            Image *b = new Image(l2);
            Image *c(a);
            *c -= *b;
            c->save(o1);
            break;
        }
        case 'l':{
            //Done
            l1 = argv[2];
            l2 = argv[3];
            o1 = argv[4];
            Image *i = new Image(l1);
            Image *j = new Image(l2);
            *i / *j;
            i->save(o1);
            break;
        }
        case 'i':{
            //Done
            l1 = argv[2];
            o1 = argv[3];
            Image *a = new Image(l1);
            !*a;
            a->save(o1);
            break;
        }
        case 't':{
            //Done
            l1 = argv[2];
            l2 = argv[3];
            o1 = argv[4];
            int thresh = stoi(l2);
            Image *a = new Image(l1);
            *a * thresh;
            a->save(o1);
            break;
        }
    }
        

}