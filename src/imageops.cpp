/**
 * 
 * Functional Description for the imageops program.
 * Matteo Kalogirou
 * 
 * */

#include "imageops.h"



//Helper methods for Image class
unsigned char Image::get(int i){ return data.get()[i]; }
int Image::getW(){ return width;}
int Image::getH(){ return height;}
void Image::printData(){
    int n = width*height;
    std::cout << "Height = " << height << " Width = " << width << std::endl;
    for (int i =0; i<n; i++)
        std::cout << (int)(data.get()[i]) << ", ";
}



//---CONSTRUCTOR
Image::Image() : width(0), height(0){}                                         //Default
Image::Image(std::string filename) : width(0), height(0) { load(filename); }   //Load from File
Image::Image(std::vector<unsigned char> &v )                                   //Load from vector
{
    height = v.size(); width = 1;
    std::unique_ptr<unsigned char []> up( new unsigned char[v.size()]);
    for(unsigned int i =0; i< v.size(); i++)
        up.get()[i] = v[i];
    data = move(up);
}



//---DESTRUCTOR
Image::~Image() { 
    if(!data)
        data.release();
}

/** --- COPY CONSTRUCTOR
 * Create a deep copy of the image.
 **/
Image::Image(const Image & rhs) : width(rhs.width), height(rhs.height)
{
    data.reset(new unsigned char[width*height]() );

    iterator beg = this->begin(), end = this->end();
    iterator inStart = rhs.begin();

    while( beg != end){
        beg = inStart;
        ++beg; ++inStart;
    }
}

/**---MOVE CONSTRUCTOR
 * Move the contents of the rhs image into the newly created image 
 **/
Image::Image(Image && rhs) : width(rhs.width), height(rhs.height), data(move(rhs.data)) {}

/**---COPY ASSIGNMENT
 * Delete the current values of the object and reassign them to the rhs values.
 * Deep copy onto already ecisting object.
**/
Image & Image::operator=(const Image & rhs){
    width = rhs.width;
    height = rhs.height;
    data.reset(new unsigned char[width*height]() );

    iterator beg = this->begin(), end = this->end();
    iterator inStart = rhs.begin();

    while( beg != end){
        beg = inStart;
        ++beg; ++inStart;
    }
    return *this;
}

/**---MOVE ASSIGNMENT
     * Move the already existing data from the rhs image into this image and leave rhs in 
     * a destructable state
     **/
Image & Image::operator=(Image && rhs){
    width = rhs.width;
    height = rhs.height;
    data = move(rhs.data);
    return *this;
}

/**---LOAD METHOD
 * Method used to laod filename.pgm into the Image class
 */
void Image::load(std::string filename)
{
    using namespace std;
    //Create an fstream object to read in
    std::ifstream in(IMGPATH+filename);
    if( in.fail() ){
        std::cout << "File: "<<filename << " not found" << std::endl;
    }else{
        std::string temp;
        char *cstr;

        std::getline(in, temp);                 //Read in P5
        std::getline(in, temp);                 //Discard all comment lines
        while(temp[0] == '#')
            getline(in, temp);

        istringstream  iss(temp);               //Store the height and width
        int w,h, n;
        iss >> h >> w >> n >> ws;
        height = h; width = w;

        cstr = new char[h*w];                   //Temporarily store the data as char
        in.read(cstr, h*w);                     
        data.reset((unsigned char*)cstr);
        
        in.close();
    }
}

/**---SAVE METHOD
 * Save the file to the output folder specified by OUTPATH.
 * */
void Image::save(std::string filename){
    using namespace std;
    //create an output filestream image and write to the file
    std::ofstream ofs(OUTPATH+filename);

    ofs << "P5\n";
    ofs << "#This is a comment\n";
    ofs << height << " "<<width << endl;
    ofs << 255 << endl;

    for(int i=0; i<width*height; i++)
        ofs << data.get()[i];

    ofs.close();
    cout << "Saved image in: "<< OUTPATH+filename << endl;
}

//==============================OVERLOADS=======================================

// Addition: return a new image that is the sum of the two input arguments
Image & Image::operator+=(const Image & rhs){
    if(height == rhs.height && width==rhs.width){

        iterator beg = this->begin(), end = this->end();
        iterator inStart = rhs.begin();

        while( beg != end){
            beg += inStart;
            ++beg; ++inStart;
        }
    }
    return *this;
}

Image Image::operator+(const Image &rhs){
    if(height == rhs.height && width==rhs.width){
        Image I(*this);
        return I+=rhs;
    }else 
        return *this;
}

// Subtraction: return a new image that is the rhs image subtracted from the lhs
Image & Image::operator-=(const Image &rhs){
    if(height == rhs.height && width==rhs.width){
        
        iterator beg = this->begin(), end = this->end();
        iterator inStart = rhs.begin();

        while( beg != end){
            beg -= inStart;
            ++beg; ++inStart;
        }
    }
    return *this;
}

Image Image::operator-(const Image &rhs){
    if(height == rhs.height && width==rhs.width){
        Image I(*this);
        return I-=rhs;
    } else
        return *this;
}

// Invert: invert according to ( 255-pixelIntesity )
Image & Image::operator!(void){

    iterator beg = this->begin(), end = this->end();

    while(beg != end){
        !beg; 
        ++beg;
    }
    return *this;
}

// Mask
Image & Image::operator/(const Image &rhs){
    iterator beg = this->begin(), end = this->end();
    iterator inStart = rhs.begin();

    do{
        if(*inStart == 0)
            beg = 0;
        ++beg; ++inStart;
    }while(beg != end);

    return *this;
}

//Threshold - if any value is below the threshold set to 0
Image & Image::operator*(const int f){
    std::cout << "In thresh" << std::endl;
    iterator beg = this->begin(), end = this->end();
    do{
        if( beg < f)
            beg = 0;
        ++beg;
    }while(beg != end);

    return *this;
}

//==============================================================================