

#ifndef _imageops
#define _imageops

#include<iostream>
#include<fstream>
#include<memory>        //for smart pointers
#include<string> 
#include<sstream>
#include<vector>

const static std::string IMGPATH = "../Images/";
const static std::string OUTPATH = "../out/";

//Classes
class Image{
    private:
        int width, height;
        std::unique_ptr<unsigned char []> data;

    public:
        //========================INNER ITERATOR CLASS===================================
        class iterator{
            private:
                unsigned char *ptr;

                //Constructor - can only be constructed using the Image methods begin()/end()
                iterator(unsigned char *p) : ptr(p) {}

            public:
                //Copy Consructor
                iterator(const iterator & rhs) : ptr(rhs.ptr) {}
                
                //---Overloads *, ++, --, =
                unsigned char operator*(){
                    return *ptr;
                }



                //---Inversion
                iterator & operator!(){
                    unsigned char temp = 255 - *ptr;
                    *ptr = temp;
                    return *this;
                }

                //---Prefix operator ++i
                iterator & operator++(){
                    ptr += 1;
                    return *this;
                }
                //---Postfix operator i++
                iterator operator++(int){
                    iterator i(ptr);
                    ++*this;
                    return i;
                }

                //---Decrement
                iterator & operator--(){
                    ptr-=1;
                    return *this;
                }
                iterator operator--(int){
                    iterator i(ptr);
                    --*this;
                    return i;
                }

                //---Comparison and Assignment, = == and !=
                iterator& operator=(const iterator & rhs){
                    *ptr = *(rhs.ptr);
                    return *this;
                }

                iterator& operator=(const unsigned char c){
                    *ptr = c;
                    return *this;
                }

                bool operator==(const iterator & rhs){
                    if(*ptr == *(rhs.ptr))
                       return true;
                    return false;
                }
                bool operator!=(const iterator & rhs){
                    if(*ptr ==*(rhs.ptr))
                        return false;
                    else
                        return true;
                }

                //---Arithmetical Operations += +
                iterator& operator+=(const iterator &rhs){
                    if( *ptr + *(rhs.ptr) >= 256 )
                        *ptr = 255;
                    else
                        *ptr += *(rhs.ptr);
                    return *this;
                }

                iterator& operator-=(const iterator &rhs){
                    if( *ptr - *(rhs.ptr) <= 0 )
                        *ptr = 0;
                    else
                        *ptr -= *(rhs.ptr);
                    return *this;
                }

                //---Comparisons
                bool operator<(const int f){
                    if(*ptr < f)
                        return true;
                    return false;
                }

            //Friend the Image class
            friend Image;

        };
        //==============================ITERATOR METHODS=================================
        iterator begin(void) const { return iterator(data.get()); }

        iterator end(void) const{ return iterator( data.get()+height*width); }

        //===============================================================================

        
        // For test cases
        unsigned char get(int i);
        int getW();
        int getH();
        void printData();


        //---CONSTRUCTOR
        Image();
        Image(std::string filename);
        Image(std::vector<unsigned char> &v);

        //---DESTRUCTOR
        ~Image();

        //--- COPY CONSTRUCTOR
        Image(const Image & rhs);

        //---MOVE CONSTRUCTOR
        Image(Image && rhs);

        //---COPY ASSIGNMENT
        Image & operator=(const Image & rhs);

        //---MOVE ASSIGNMENT
        Image & operator=(Image && rhs);


        //---LOAD METHOD
        void load(std::string filename);

        //---SAVE METHOD
        void save(std::string filename);

        //==============================OVERLOADS=======================================

        // Addition: return a new image that is the sum of the two input arguments
        Image & operator+=(const Image & rhs);

        Image operator+(const Image &rhs);

        // Subtraction: return a new image that is the rhs image subtracted from the lhs
        Image & operator-=(const Image &rhs);

        Image operator-(const Image &rhs);
        
        // Invert: invert according to ( 255-pixelIntesity )
        Image & operator!(void);

        //Threshold - if any value is below the threshold set to 0
        Image & operator*(const int f);

        //Mask - show the image underneath the mask only
        Image & operator/(const Image &rhs);

        //==============================================================================



};

#endif
